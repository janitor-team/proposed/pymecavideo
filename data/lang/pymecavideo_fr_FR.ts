<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="fr">
<context>
    <name>MontreFilm</name>
    <message>
        <location filename="../../src/cadreur.py" line="211"/>
        <source>Voir la vidéo</source>
        <translation>Voir la vidéo</translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="212"/>
        <source>Ralenti : 1/1</source>
        <translation>Ralenti : 1/1</translation>
    </message>
</context>
<context>
    <name>StartQT4</name>
    <message>
        <location filename="pymecavideo.py" line="308"/>
        <source>indéf</source>
        <translation type="obsolete">indéf</translation>
    </message>
    <message>
        <location filename="." line="115414494"/>
        <source>indéf.</source>
        <comment>utf8</comment>
        <translation type="obsolete">indéf.</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>temps en seconde, positions en mètre</source>
        <translation type="obsolete">temps en seconde, positions en mètre</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>point N°</source>
        <translation type="obsolete">point N°</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Evolution de l'ordonnée du point %1</source>
        <translation type="obsolete">Evolution de l'ordonnée du point %1</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Vous avez atteint la fin de la vidéo</source>
        <translation type="obsolete">Vous avez atteint la fin de la vidéo</translation>
    </message>
    <message>
        <location filename="." line="49"/>
        <source>Quelle est la longueur en mètre de votre étalon sur l'image ?</source>
        <translation type="obsolete">Quelle est la longueur en mètre de votre étalon sur l'image ?</translation>
    </message>
    <message>
        <location filename="." line="49"/>
        <source> Merci d'indiquer une échelle valable</source>
        <translation type="obsolete"> Merci d'indiquer une échelle valable</translation>
    </message>
    <message>
        <location filename="." line="38"/>
        <source>Les données seront perdues</source>
        <translation type="obsolete">Les données seront perdues</translation>
    </message>
    <message>
        <location filename="." line="38"/>
        <source>Votre travail n'a pas été sauvegardé
Voulez-vous les sauvegarder ?</source>
        <translation type="obsolete">Votre travail n'a pas été sauvegardé
Voulez-vous les sauvegarder ?</translation>
    </message>
    <message>
        <location filename="." line="22"/>
        <source>Vous avez atteint le début de la vidéo</source>
        <translation type="obsolete">Vous avez atteint le début de la vidéo</translation>
    </message>
    <message>
        <location filename="." line="14"/>
        <source>Ouvrir une vidéo</source>
        <translation type="obsolete">Ouvrir une vidéo</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="obsolete">fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Nom de fichier non conforme</source>
        <translation type="obsolete">Nom de fichier non conforme</translation>
    </message>
    <message>
        <location filename="." line="115413456"/>
        <source>Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <comment>utf8</comment>
        <translation type="obsolete">Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Veuillez choisir une image et définir l'échelle</source>
        <translation type="obsolete">Veuillez choisir une image et définir l'échelle</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>Désolé pas de fichier d'aide pour le langage %1.</source>
        <translation type="obsolete">Désolé pas de fichier d'aide pour le langage %1.</translation>
    </message>
    <message>
        <location filename="." line="0"/>
        <source>indÃ©f.</source>
        <translation type="obsolete">indéf.</translation>
    </message>
    <message>
        <location filename="." line="1232"/>
        <source>Le nom de votre fichier contient des caractÃ¨res accentuÃ©s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</translation>
    </message>
</context>
<context>
    <name>pymecavideo</name>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="37"/>
        <source>PyMecaVideo, analyse mécanique des vidéos</source>
        <translation>PyMecaVideo, analyse mécanique des vidéos</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="86"/>
        <source>Acquisition des données</source>
        <translation type="obsolete">Acquisition des données</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="123"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="102"/>
        <source>Pointage</source>
        <translation>Pointage</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="287"/>
        <source>Démarrer</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="418"/>
        <source>efface la série précédente</source>
        <translation type="obsolete">efface la série précédente</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="455"/>
        <source>rétablit le point suivant</source>
        <translation type="obsolete">rétablit le point suivant</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="384"/>
        <source>Tout réinitialiser</source>
        <translation>Tout réinitialiser</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="533"/>
        <source>Définir l'échelle</source>
        <translation>Définir l'échelle</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="480"/>
        <source>indéf.</source>
        <translation>indéf.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="621"/>
        <source>Points à 
 étudier:</source>
        <translation type="obsolete">Points à 
 étudier:</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="434"/>
        <source>Ordonnées 
vers le bas</source>
        <translation type="obsolete">Ordonnées 
vers le bas</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="58"/>
        <source>Lancer le logiciel
 d'acquisition Vidéo</source>
        <translation type="obsolete">Lancer le logiciel
 d'acquisition Vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="926"/>
        <source>Image n°</source>
        <translation>Image n°</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1056"/>
        <source>Voir la vidéo</source>
        <translation>Voir la vidéo</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="897"/>
        <source>Depuis ce référentiel</source>
        <translation type="obsolete">Depuis ce référentiel</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="875"/>
        <source>Échelle de vitesses</source>
        <translation>Échelle de vitesses</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="209"/>
        <source>Acquisition</source>
        <translation type="obsolete">Acquisition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="639"/>
        <source>suivi
automatique</source>
        <translation type="obsolete">suivi
automatique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="404"/>
        <source>Changer d&apos;origine</source>
        <translation>Changer d'origine</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2013"/>
        <source>Choisir ...</source>
        <translation>Choisir ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="868"/>
        <source>Chrono
photographie</source>
        <translation type="obsolete">Chrono
photographie</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1484"/>
        <source>Enregistrer</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1089"/>
        <source>Coordonnées</source>
        <translation>Coordonnées</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1095"/>
        <source>Tableau des dates et des coordonnées</source>
        <translation>Tableau des dates et des coordonnées</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1125"/>
        <source>Copier les mesures dans le presse papier</source>
        <translation>Copier les mesures dans le presse papier</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1058"/>
        <source>Exporter vers ....</source>
        <translation type="obsolete">Exporter vers ....</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="997"/>
        <source>Oo.o Calc</source>
        <translation type="obsolete">Oo.o Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1073"/>
        <source>Qtiplot</source>
        <translation type="obsolete">Qtiplot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1078"/>
        <source>SciDAVis</source>
        <translation type="obsolete">SciDAVis</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1015"/>
        <source>changer d'échelle ?</source>
        <translation type="obsolete">changer d'échelle ?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1559"/>
        <source>&amp;Fichier</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1563"/>
        <source>E&amp;xporter vers ...</source>
        <translation>E&amp;xporter vers ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1670"/>
        <source>&amp;Aide</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1591"/>
        <source>&amp;Edition</source>
        <translation>&amp;Edition</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1102"/>
        <source>&amp;Ouvrir une vidéo (Ctrl-O)</source>
        <translation type="obsolete">&amp;Ouvrir une vidéo (Ctrl-O)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1620"/>
        <source>avanceimage</source>
        <translation>avanceimage</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1625"/>
        <source>reculeimage</source>
        <translation>reculeimage</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1120"/>
        <source>Quitter (Ctrl-Q)</source>
        <translation type="obsolete">Quitter (Ctrl-Q)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1128"/>
        <source>Enregistrer les données (Ctrl-S)</source>
        <translation type="obsolete">Enregistrer les données (Ctrl-S)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1661"/>
        <source>À &amp;propos</source>
        <translation>À &amp;propos</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1141"/>
        <source>Aide (F1)</source>
        <translation type="obsolete">Aide (F1)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1149"/>
        <source>Exemples ...</source>
        <translation type="obsolete">Exemples ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1154"/>
        <source>Ouvrir un projet &amp;mecavidéo</source>
        <translation type="obsolete">Ouvrir un projet &amp;mecavidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1696"/>
        <source>&amp;Préférences</source>
        <translation>&amp;Préférences</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1531"/>
        <source>&amp;Copier dans le presse-papier (Ctrl-C)</source>
        <translation type="obsolete">&amp;Copier dans le presse-papier (Ctrl-C)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1172"/>
        <source>Défaire (Ctrl-Z)</source>
        <translation type="obsolete">Défaire (Ctrl-Z)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1180"/>
        <source>Refaire (Ctrl-Y)</source>
        <translation type="obsolete">Refaire (Ctrl-Y)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="1188"/>
        <source>OpenOffice.org &amp;Calc</source>
        <translation type="obsolete">OpenOffice.org &amp;Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1748"/>
        <source>Qti&amp;plot</source>
        <translation>Qti&amp;plot</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1753"/>
        <source>Sci&amp;davis</source>
        <translation>Sci&amp;davis</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="189"/>
        <source>Pas de vidéos chargées</source>
        <translation type="obsolete">Pas de vidéos chargées</translation>
    </message>
    <message>
        <location filename="pymecavideo_mini.ui" line="280"/>
        <source>Acquisition video</source>
        <translation type="obsolete">Acquisition vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="782"/>
        <source>Montrer 
les vecteurs
vitesses</source>
        <translation type="obsolete">Montrer 
les vecteurs
vitesses</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="791"/>
        <source>près de
la souris</source>
        <translation type="obsolete">près de
la souris</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="852"/>
        <source>partout</source>
        <translation>partout</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini.ui" line="966"/>
        <source>Définir un autre référentiel : </source>
        <translation type="obsolete">Définir un autre référentiel : </translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="314"/>
        <source>Lancer %1
 pour capturer une vidÃ©o</source>
        <translation type="obsolete">Lancer %1
 pour capturer une vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="387"/>
        <source>indÃ©f</source>
        <translation type="obsolete">indéf</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="332"/>
        <source>NON DISPO : %1</source>
        <translation type="obsolete">NON DISPO : %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1125"/>
        <source>Ouvrir un projet Pymecavideo</source>
        <translation>Ouvrir un projet Pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1221"/>
        <source>fichiers pymecavideo(*.csv)</source>
        <translation type="obsolete">fichiers pymecavideo(*.csv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1397"/>
        <source>temps en seconde, positions en mÃ¨tre</source>
        <translation type="obsolete">temps en seconde, positions en mètre</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1245"/>
        <source>point NÂ° %1</source>
        <translation type="obsolete">point N° %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1452"/>
        <source>Veuillez sÃ©lectionner un cadre autour de(s) l'objet(s) que vous voulez suivre.
Vous pouvez arrÃªter Ã  tous moments la capture en appuyant sur le bouton</source>
        <translation type="obsolete">Veuillez sélectionner un cadre autour de(s) l'objet(s) que vous voulez suivre.
Vous pouvez arrêter à tous moments la capture en appuyant sur le bouton</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de l&apos;abscisse du point %1</source>
        <translation type="obsolete">Evolution de l'abscisse du point %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de l'ordonnÃ©e du point %1</source>
        <translation type="obsolete">Evolution de l'ordonnée du point %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1432"/>
        <source>Evolution de la vitesse du point %1</source>
        <translation type="obsolete">Evolution de la vitesse du point %1</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1496"/>
        <source>Pointage des positionsÂ : cliquer sur le point NÂ° %1</source>
        <translation type="obsolete">Pointage des positions : cliquer sur le point N° %1</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2204"/>
        <source>Vous avez atteint la fin de la vidÃ©o</source>
        <translation type="obsolete">Vous avez atteint la fin de la vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2551"/>
        <source>DÃ©finir une Ã©chelle</source>
        <translation type="obsolete">Définir une échelle</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2530"/>
        <source>Quelle est la longueur en mÃ¨tre de votre Ã©talon sur l'image ?</source>
        <translation type="obsolete">Quelle est la longueur en mètre de votre étalon sur l'image ?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2552"/>
        <source> Merci d'indiquer une Ã©chelle valable</source>
        <translation type="obsolete"> Merci d'indiquer une échelle valable</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Les donnÃ©es seront perdues</source>
        <translation type="obsolete">Les données seront perdues</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2613"/>
        <source>Votre travail n'a pas Ã©tÃ© sauvegardÃ©
Voulez-vous les sauvegarder ?</source>
        <translation type="obsolete">Votre travail n'a pas été sauvegardé
Voulez-vous les sauvegarder ?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1782"/>
        <source>Vous avez atteint le dÃ©but de la vidÃ©o</source>
        <translation type="obsolete">Vous avez atteint le début de la vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2707"/>
        <source>Ouvrir une vidÃ©o</source>
        <translation type="obsolete">Ouvrir une vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2762"/>
        <source>Nom de fichier non conforme</source>
        <translation>Nom de fichier non conforme</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2702"/>
        <source>Le nom de votre fichier contient des caractÃ¨res accentuÃ©s ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation type="obsolete">Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1862"/>
        <source>Veuillez choisir une image et dÃ©finir l'Ã©chelle</source>
        <translation type="obsolete">Veuillez choisir une image et définir l'échelle</translation>
    </message>
    <message>
        <location filename="pymecavideo.py" line="1907"/>
        <source>DÃ©solÃ© pas de fichier d'aide pour le langage %1.</source>
        <translation type="obsolete">Désolé pas de fichier d'aide pour le langage %1.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1798"/>
        <source>fichiers vidÃ©os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation type="obsolete">fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2688"/>
        <source>fichiers vidÃ©os ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation type="obsolete">fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="660"/>
        <source>Enregistrer la chronophotographie</source>
        <translation type="obsolete">Enregistrer la chronophotographie</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="688"/>
        <source>fichiers images(*.png *.jpg)</source>
        <translation>fichiers images(*.png *.jpg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="446"/>
        <source>NON DISPO : {0}</source>
        <translation type="obsolete">NON DISPO : {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1495"/>
        <source>point N° {0}</source>
        <translation>point N° {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2016"/>
        <source>Evolution de l&apos;abscisse du point {0}</source>
        <translation type="obsolete">Evolution de l'abscisse du point {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1446"/>
        <source>Evolution de l'ordonnée du point {0}</source>
        <translation type="obsolete">Evolution de l'ordonnée du point {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2016"/>
        <source>Evolution de la vitesse du point {0}</source>
        <translation type="obsolete">Evolution de la vitesse du point {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2227"/>
        <source>Pointage des positions : cliquer sur le point N° {0}</source>
        <translation type="obsolete">Pointage des positions : cliquer sur le point N° {0}</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2833"/>
        <source>Désolé pas de fichier d'aide pour le langage {0}.</source>
        <translation>Désolé pas de fichier d'aide pour le langage {0}.</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2731"/>
        <source>fichiers vidéos (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</source>
        <translation>fichiers vidéos (*.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.mov *.wmv)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2798"/>
        <source>Veuillez choisir une image (et définir l'échelle)</source>
        <translation>Veuillez choisir une image (et définir l'échelle)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2913"/>
        <source>Pymecavideo n'arrive pas à lire l'image</source>
        <translation>Pymecavideo n'arrive pas à lire l'image</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="882"/>
        <source>Fichier Python créé</source>
        <translation type="obsolete">Fichier Python créé</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1519"/>
        <source>Pointage Automatique</source>
        <translation>Pointage Automatique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1114"/>
        <source>le format de cette vidéo n'est pas pris en charge par pymecavideo</source>
        <translation>le format de cette vidéo n'est pas pris en charge par pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1293"/>
        <source>Le fichier choisi n&apos;est pas compatible avec pymecavideo</source>
        <translation>Le fichier choisi n'est pas compatible avec pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2385"/>
        <source>Masse de l&apos;objet</source>
        <translation>Masse de l'objet</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2385"/>
        <source>Quelle est la masse de l&apos;objet ? (en kg)</source>
        <translation>Quelle est la masse de l'objet ? (en kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2403"/>
        <source> Merci d&apos;indiquer une masse valable</source>
        <translation> Merci d'indiquer une masse valable</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2709"/>
        <source>MAUVAISE VALEUR !</source>
        <translation>MAUVAISE VALEUR !</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2709"/>
        <source>La valeur rentrée n'est pas compatible avec le calcul</source>
        <translation>La valeur rentrée n'est pas compatible avec le calcul</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="236"/>
        <source>px/m</source>
        <translation>px/m</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="414"/>
        <source>Abscisses 
vers la gauche</source>
        <translation type="unfinished">Abscisses 
vers la gauche</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="787"/>
        <source>Trajectoires</source>
        <translation>Trajectoires</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="842"/>
        <source>près de la souris</source>
        <translation>près de la souris</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="882"/>
        <source>px pour 1 m/s</source>
        <translation>px pour 1 m/s</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1379"/>
        <source>9.8</source>
        <translation>9.8</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1326"/>
        <source>Grapheur</source>
        <translation>Grapheur</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1423"/>
        <source>en fonction de </source>
        <translation>en fonction de </translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1682"/>
        <source>&amp;Exemples ...</source>
        <translation>&amp;Exemples ...</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1743"/>
        <source>LibreOffice &amp;Calc</source>
        <translation>LibreOffice &amp;Calc</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1758"/>
        <source>&amp;Python (source)</source>
        <translation>&amp;Python (source)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="451"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nombre d&apos;Images Par Seconde&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Nombre d'Images Par Seconde&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="454"/>
        <source>IPS :</source>
        <translation>IPS :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="476"/>
        <source>000</source>
        <translation>000</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="604"/>
        <source>Tourner l'image de 90° vers la gauche</source>
        <translation>Tourner l'image de 90° vers la gauche</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="633"/>
        <source>Tourner l'image de 90° vers la droite</source>
        <translation>Tourner l'image de 90° vers la droite</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="815"/>
        <source>Trajectoire</source>
        <translation>Trajectoire</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="912"/>
        <source>Chronophotographie</source>
        <translation>Chronophotographie</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="825"/>
        <source>Chronogramme</source>
        <translation>Chronogramme</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="846"/>
        <source>Montrer les
vecteurs vitesses</source>
        <translation type="obsolete">Montrer les
vecteurs vitesses</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1359"/>
        <source>1.0</source>
        <translation>1.0</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1260"/>
        <source>g (N/kg)</source>
        <translation>g (N/kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1433"/>
        <source>avec le style </source>
        <translation>avec le style </translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1441"/>
        <source>Points seuls</source>
        <translation>Points seuls</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1450"/>
        <source>Points et lignes</source>
        <translation>Points et lignes</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1459"/>
        <source>Lignes seules</source>
        <translation>Lignes seules</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1763"/>
        <source>&amp;Fichier numpy</source>
        <translation>&amp;Fichier numpy</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1766"/>
        <source>Fichier Numpy</source>
        <translation>Fichier Numpy</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="688"/>
        <source>Enregistrer comme image</source>
        <translation>Enregistrer comme image</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2176"/>
        <source>Erreur lors de l&apos;enregistrement</source>
        <translation>Erreur lors de l'enregistrement</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2587"/>
        <source>Définir léchelle</source>
        <translation>Définir léchelle</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2858"/>
        <source>Le nombre d'images par seconde doit être un entier</source>
        <translation>Le nombre d'images par seconde doit être un entier</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2858"/>
        <source>merci de recommencer</source>
        <translation>merci de recommencer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="297"/>
        <source>STOP</source>
        <translation>STOP</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="707"/>
        <source>Efface le point précédent</source>
        <translation>Efface le point précédent</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="745"/>
        <source>Rétablit le point suivant</source>
        <translation>Rétablit le point suivant</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1521"/>
        <source>Veuillez sélectionner un cadre autour du ou des objets que vous voulez suivre.
Vous pouvez arrêter à tout moment la capture en appuyant sur le bouton STOP</source>
        <translation>Veuillez sélectionner un cadre autour du ou des objets que vous voulez suivre.
Vous pouvez arrêter à tout moment la capture en appuyant sur le bouton STOP</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1612"/>
        <source>&amp;Ouvrir une vidéo</source>
        <translation>&amp;Ouvrir une vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1634"/>
        <source>&amp;Quitter</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1705"/>
        <source>&amp;Copier dans le presse-papier</source>
        <translation>&amp;Copier dans le presse-papier</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1717"/>
        <source>&amp;Défaire</source>
        <translation>&amp;Défaire</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1735"/>
        <source>Refaire</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1729"/>
        <source>&amp;Refaire</source>
        <translation>&amp;Refaire</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1183"/>
        <source>Changer d'échelle</source>
        <translation>Changer d'échelle</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1413"/>
        <source>Tracer :</source>
        <translation>Tracer :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1208"/>
        <source>Ajouter les énergies :</source>
        <translation>Ajouter les énergies :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1228"/>
        <source>Cinétique (échelle obligatoire)</source>
        <translation>Cinétique (échelle obligatoire)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1235"/>
        <source>Potentielle de pesanteur</source>
        <translation>Potentielle de pesanteur</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1242"/>
        <source>Mécanique</source>
        <translation>Mécanique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="277"/>
        <source>Suivi automatique</source>
        <translation>Suivi automatique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="254"/>
        <source>Points à 
étudier :</source>
        <translation type="unfinished">Points à 
étudier :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1215"/>
        <source>Intensité de la pesanteur :</source>
        <translation>Intensité de la pesanteur :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1338"/>
        <source>Données et grandeurs à représenter</source>
        <translation>Données et grandeurs à représenter</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1346"/>
        <source>Masse (kg)</source>
        <translation>Masse (kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1366"/>
        <source>Intensité de la pesanteur g (N/kg)</source>
        <translation>Intensité de la pesanteur g (N/kg)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1006"/>
        <source>Changement de référentiel : </source>
        <translation>Changement de référentiel : </translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1646"/>
        <source>&amp;Enregistrer le projet mecavideo</source>
        <translation>&amp;Enregistrer le projet mecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1691"/>
        <source>Ouvrir un projet &amp;mecavideo</source>
        <translation>Ouvrir un projet &amp;mecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2176"/>
        <source>Echec de l&apos;enregistrement du fichier:&lt;b&gt;
{0}&lt;/b&gt;</source>
        <translation>Echec de l'enregistrement du fichier:&amp;lt;b&amp;gt;
{0}&amp;lt;/b&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2168"/>
        <source>Enregistrer le graphique</source>
        <translation>Enregistrer le graphique</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1125"/>
        <source>Projet Pymecavideo (*.mecavideo)</source>
        <translation>Projet Pymecavideo (*.mecavideo)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1441"/>
        <source>Enregistrer le projet pymecavideo</source>
        <translation>Enregistrer le projet pymecavideo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1441"/>
        <source>Projet pymecavideo (*.mecavideo)</source>
        <translation>Projet pymecavideo (*.mecavideo)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2168"/>
        <source>fichiers images(*.png)</source>
        <translation>fichiers images (*.png)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="395"/>
        <source>indéf</source>
        <translation>indéf</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="1420"/>
        <source>temps en seconde, positions en mètre</source>
        <translation>temps en seconde, positions en mètre</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2254"/>
        <source>Vous avez atteint la fin de la vidéo</source>
        <translation>Vous avez atteint la fin de la vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2587"/>
        <source>Quelle est la longueur en mètre de votre étalon sur l'image ?</source>
        <translation>Quelle est la longueur en mètre de votre étalon sur l'image ?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2610"/>
        <source> Merci d'indiquer une échelle valable</source>
        <translation> Merci d'indiquer une échelle valable</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2670"/>
        <source>Les données seront perdues</source>
        <translation>Les données seront perdues</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2670"/>
        <source>Votre travail n'a pas été sauvegardé
Voulez-vous les sauvegarder ?</source>
        <translation>Votre travail n'a pas été sauvegardé
Voulez-vous les sauvegarder ?</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2767"/>
        <source>Ouvrir une vidéo</source>
        <translation>Ouvrir une vidéo</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2747"/>
        <source>fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</source>
        <translation>fichiers vidéos ( *.avi *.mp4 *.ogv *.mpg *.mpeg *.ogg *.wmv *.mov)</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2762"/>
        <source>Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</source>
        <translation>Le nom de votre fichier contient des caractères accentués ou des espaces.
Merci de bien vouloir le renommer avant de continuer</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="414"/>
        <source>Abscisses 
<byte value="x9"/><byte value="x9"/><byte value="x9"/>    vers la gauche</source>
        <translation type="obsolete">Abscisses 
			    vers la gauche</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="434"/>
        <source>Ordonnées 
			    vers le bas</source>
        <translation type="obsolete">Ordonnées 
			    vers le bas</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="528"/>
        <source>Incrémenter le compteur d'image à chaque pointage</source>
        <translation>Incrémenter le compteur d'image à chaque pointage</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="518"/>
        <source>Incr :</source>
        <translation>Incr :</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="892"/>
        <source>Montrer les
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>  vecteurs vitesses</source>
        <translation>Montrer les
				  vecteurs vitesses</translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="434"/>
        <source>Ordonnées 
 vers le bas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo.py" line="2233"/>
        <source>Pointage des positions : cliquer sur le point N° {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="473"/>
        <source>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Le nombre d'images par seconde est détecté automatiquement. Enter la valeur manuellement si la détection échoue.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/pymecavideo_mini_layout.ui" line="1649"/>
        <source>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;Enregistre les données du projet pour pouvoir être réouvert dans PyMecaVideo.&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.app</name>
    <message>
        <location filename="../../src/cadreur.py" line="55"/>
        <source>Presser la touche ESC pour sortir</source>
        <translation>Presser la touche ESC pour sortir</translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="167"/>
        <source>Choisir le nombre de points puis Â«Â DÃ©marrer l'acquisitionÂ Â» </source>
        <translation type="obsolete">Choisir le nombre de points puis « Démarrer l'acquisition » </translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="188"/>
        <source>Vous pouvez continuer votre acquisition</source>
        <translation>Vous pouvez continuer votre acquisition</translation>
    </message>
    <message>
        <location filename="preferences.py" line="48"/>
        <source>Proximite de la souris %1</source>
        <translation type="obsolete">Proximité de la souris %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="49"/>
        <source>; derniere video %1</source>
        <translation type="obsolete">; dernière vidéo %1</translation>
    </message>
    <message>
        <location filename="preferences.py" line="50"/>
        <source>; videoDir %1</source>
        <translation type="obsolete">; videoDir %1</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="47"/>
        <source>Proximite de la souris {0}</source>
        <translation>Proximite de la souris {0}</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="49"/>
        <source>; derniere video {0}</source>
        <translation>; derniere video {0}</translation>
    </message>
    <message>
        <location filename="../../src/preferences.py" line="50"/>
        <source>; videoDir {0}</source>
        <translation>; videoDir {0}</translation>
    </message>
    <message>
        <location filename="../../src/cadreur.py" line="143"/>
        <source>Choisir le ralenti</source>
        <translation type="obsolete">Choisir le ralenti</translation>
    </message>
    <message>
        <location filename="../../src/echelle.py" line="180"/>
        <source>Choisir le nombre de points puis « Démarrer l'acquisition » </source>
        <translation>Choisir le nombre de points puis « Démarrer l'acquisition » </translation>
    </message>
</context>
</TS>
